const icon = document.querySelectorAll('.icon-password');
icon.forEach(icon => {
    icon.addEventListener('click', () => {
        const input = icon.parentElement.querySelector('input');
        if (input.type === 'password') {
            input.type = 'text';
            icon.classList.remove('fa-eye');
            icon.classList.add('fa-eye-slash');
        } else {
            input.type = 'password';
            icon.classList.remove('fa-eye-slash');
            icon.classList.add('fa-eye');
        }
    });
});

const form = document.querySelector('.password-form');
form.addEventListener('submit', (event) => {
    event.preventDefault();

    const passInput = form.querySelector('.input');
    const passConfirmInput = form.querySelector('.input2');

    if (passInput.value === passConfirmInput.value) {
        alert('You are welcome');
    } else {
        const error = document.createElement('span');
        error.textContent = 'Потрібно ввести однакові значення';
        error.style.color = 'red';
        const wrapper = passConfirmInput.parentElement;
        wrapper.appendChild(error);
        passInput.value = ''
        passConfirmInput.value = ''
    }
}, {once: true});